﻿using System.Web.Mvc;

namespace AnonymousChat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}