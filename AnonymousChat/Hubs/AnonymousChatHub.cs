﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AnonymousChat.Models.Entities;
using AnonymousChat.Models.Interfaces;
using Microsoft.AspNet.SignalR;
using Ninject;

namespace AnonymousChat.Hubs
{
    public class AnonymousChatHub : Hub
    {
        [Inject]
        public IGenerator<long> IdGenerator { get; set; }

        [Inject]
        public IRepository<long, Message> Messages { get; set; }

        [Inject]
        public IRepository<string, User> Users { get; set; }

        public void Send(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                var user = Users.Get(Context.ConnectionId);
                var message = new Message {User = user.Name, Text = text, Time = DateTime.UtcNow};
                Messages.Save(message);
                Clients.Caller.addMyMessage(user.Name, message.Time.ToString("g"), message.Text);
                Clients.Others.addMessage(user.Name, message.Time.ToString("g"), message.Text);
            }
        }

        public override Task OnConnected()
        {
            var user = new User
            {
                Id = Context.ConnectionId,
                Name = "anonymous #" + IdGenerator.Next(),
                ConnectedAt = DateTime.UtcNow
            };

            Users.Save(user);
            Clients.Caller.onConnected(user.Id, user.Name,
                                       Users.GetAll()
                                            .OrderBy(x => x.ConnectedAt)
                                            .Select(
                                                x => new {id = x.Id, name = x.Name, time = x.ConnectedAt.ToString("g")}));

            Clients.AllExcept(user.Id).onNewUserConnected(user.Id, user.Name, user.ConnectedAt.ToString("g"));

            foreach (var m in Messages.GetAll())
            {
                Clients.Caller.addMessage(m.User, m.Time.ToString("g"), m.Text);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Users.Delete(Context.ConnectionId);
            Clients.All.onUserDisconnected(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}