﻿using AnonymousChat.Models.Interfaces;

namespace AnonymousChat.Models
{
    public class Counter : IGenerator<long>
    {
        private long _counter;

        public Counter(long initial = 0)
        {
            _counter = initial;
        }

        public long Next()
        {
            return _counter++;
        }
    }
}