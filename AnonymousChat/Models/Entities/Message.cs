﻿using System;
using AnonymousChat.Models.Interfaces;

namespace AnonymousChat.Models.Entities
{
    public class Message : IEntity<long>
    {
        public string Text { get; set; }

        public DateTime Time { get; set; }

        public string User { get; set; }

        public long Id { get; set; }
    }
}