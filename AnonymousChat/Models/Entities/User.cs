﻿using System;
using AnonymousChat.Models.Interfaces;

namespace AnonymousChat.Models.Entities
{
    public class User : IEntity<string>
    {
        public DateTime ConnectedAt { get; set; }

        public string Name { get; set; }

        public string Id { get; set; }
    }
}