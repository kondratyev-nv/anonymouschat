﻿namespace AnonymousChat.Models.Interfaces
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}