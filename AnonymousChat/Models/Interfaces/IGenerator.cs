﻿namespace AnonymousChat.Models.Interfaces
{
    public interface IGenerator<out T>
    {
        T Next();
    }
}