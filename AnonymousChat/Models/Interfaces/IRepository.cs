﻿using System.Linq;

namespace AnonymousChat.Models.Interfaces
{
    public interface IRepository<TId, TEntity> where TEntity : class, IEntity<TId>
    {
        void Save(TEntity item);

        void Update(TEntity item);

        void Delete(TEntity item);

        void Delete(TId id);

        TEntity Get(TId id);

        IQueryable<TEntity> GetAll();
    }
}