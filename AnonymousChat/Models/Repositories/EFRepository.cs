﻿using System.Data.Entity;
using System.Linq;
using AnonymousChat.Models.Interfaces;

namespace AnonymousChat.Models.Repositories
{
    public class EFRepository<TId, TEntity> : DbContext, IRepository<TId, TEntity> where TEntity : class, IEntity<TId>
    {
        public EFRepository(string name) : base(name)
        {
        }

        public DbSet<TEntity> DbSet { get; set; }

        public void Save(TEntity entity)
        {
            DbSet.Add(entity);
            SaveChanges();
        }

        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public TEntity Get(TId id)
        {
            return DbSet.SingleOrDefault(x => x.Id.Equals(id));
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
            SaveChanges();
        }

        public void Delete(TId id)
        {
            Delete(Get(id));
        }
    }
}