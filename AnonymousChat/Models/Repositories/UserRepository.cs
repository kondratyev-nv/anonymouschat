﻿using System.Collections.Concurrent;
using System.Linq;
using AnonymousChat.Models.Entities;
using AnonymousChat.Models.Interfaces;

namespace AnonymousChat.Models.Repositories
{
    public class UserRepository : IRepository<string, User>
    {
        private readonly ConcurrentDictionary<string, User> users = new ConcurrentDictionary<string, User>();

        public User Get(string id)
        {
            User user;
            users.TryGetValue(id, out user);
            return user;
        }

        public void Save(User item)
        {
            users.TryAdd(item.Id, item);
        }

        public void Update(User item)
        {
            User user;
            users.TryGetValue(item.Id, out user);
            users.TryUpdate(item.Id, item, user);
        }

        public void Delete(User item)
        {
            Delete(item.Id);
        }

        public void Delete(string id)
        {
            User user;
            users.TryRemove(id, out user);
        }

        public IQueryable<User> GetAll()
        {
            return users.Values.AsQueryable();
        }
    }
}