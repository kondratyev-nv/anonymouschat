﻿
$(function () {

    waitingDialog.show();

    var chat = $.connection.anonymousChatHub;

    chat.client.addMessage = showMessage;

    chat.client.addMyMessage = showMyMessage;

    $('#send_button').on("click", function (e) {
        e.preventDefault();
        chat.server.send($('#text').val());
        $('#text').val('');
    });

    chat.client.onConnected = function (id, userName, allUsers) {

        $('#username').append("You are " + '<b>' + userName + "</b>");

        for (i = 0; i < allUsers.length; i++) {
            addUser(
                allUsers[i].id,
                allUsers[i].name,
                allUsers[i].time
            );
        }
    }

    chat.client.onNewUserConnected = addUser;

    chat.client.onUserDisconnected = removeUser;

    $.connection.hub.start().done(function () {
        waitingDialog.hide();
    });
});

function showMessage(name, time, text) {
    $('#messages').append('<div class="well well-sm" ><p>' + text + '</p><small class="text-muted"> by ' + name + ', ' + time + ' UTC</small></div>');
    $('#messages').scrollTop($('#messages').prop('scrollHeight'));
}

function showMyMessage(name, time, text) {
    $('#messages').append('<div class="well well-sm mymessage" ><p>' + text + '</p><small class="text-muted"> by ' + name + ', ' + time + ' UTC</small></div>');
    $('#messages').scrollTop($('#messages').prop('scrollHeight'));
}

function addUser(id, name, time) {
    $("#users").append('<p id="' + id + '">' + name + '<br><small class="text-muted"> connected at ' + time + ' UTC</small></p>');
}

function removeUser(id) {
    $('#' + id).remove();
}
